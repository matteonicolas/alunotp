Último commit realizado foi por Achilleas Pipinellis. 
A linguagem utilizada foi JavaScript.





matte@Matteo MINGW64 ~/Desktop/2° semestre ADS/Técnicas de programação/exercicio
$ pwd
/c/Users/matte/Desktop/2° semestre ADS/Técnicas de programação/exercicio

matte@Matteo MINGW64 ~/Desktop/2° semestre ADS/Técnicas de programação/exercicio
$ ls
atividade-do-gobbatao/

matte@Matteo MINGW64 ~/Desktop/2° semestre ADS/Técnicas de programação/exercicio
$ git clone https://gitlab.com/pages/plain-html.git
Cloning into 'plain-html'...
remote: Enumerating objects: 20, done.
remote: Counting objects: 100% (20/20), done.
remote: Compressing objects: 100% (20/20), done.
remote: Total 69 (delta 4), reused 0 (delta 0), pack-reused 49
Receiving objects: 100% (69/69), 11.30 KiB | 2.26 MiB/s, done.
Resolving deltas: 100% (14/14), done.

matte@Matteo MINGW64 ~/Desktop/2° semestre ADS/Técnicas de programação/exercicio
$ ls
atividade-do-gobbatao/  plain-html/

matte@Matteo MINGW64 ~/Desktop/2° semestre ADS/Técnicas de programação/exercicio
$ cd plain-html/

matte@Matteo MINGW64 ~/Desktop/2° semestre ADS/Técnicas de programação/exercicio/plain-html (master)
$ git --oneline
unknown option: --oneline
usage: git [--version] [--help] [-C <path>] [-c <name>=<value>]
           [--exec-path[=<path>]] [--html-path] [--man-path] [--info-path]
           [-p | --paginate | -P | --no-pager] [--no-replace-objects] [--bare]
           [--git-dir=<path>] [--work-tree=<path>] [--namespace=<name>]
           <command> [<args>]

matte@Matteo MINGW64 ~/Desktop/2° semestre ADS/Técnicas de programação/exercicio/plain-html (master)
$ git log
commit ff960430d8735060c8b89668205b47f20538b5b6 (HEAD -> master, origin/master, origin/HEAD)
Merge: f4718b9 45e4dd4
Author: Achilleas Pipinellis <axil@gitlab.com>
Date:   Mon Nov 23 12:52:03 2020 +0000

    Merge branch '12-question-why-repo-size-is-almost-200-mb' into 'master'

    Resolve "[question] why repo size is almost 200 mb ?"

    Closes #12

    See merge request pages/plain-html!150

commit 45e4dd4e2a29e8fbfb1cfc4ae99c231a31e8ce77
Author: Achilleas Pipinellis <axil@gitlab.com>
Date:   Mon Nov 23 12:51:31 2020 +0000

    Expire artifacts in one day

commit f4718b901e8beabee16f21f56c434085ae7fbb0b
Merge: 9e6d4c7 bca4409
Author: Achilleas Pipinellis <axil@gitlab.com>
